package com.wel.springjwt.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.wel.springjwt.repository.ZodiacRepository;
import com.wel.springjwt.models.Zodiac;
import java.util.Optional;

import org.springframework.security.access.prepost.PreAuthorize;

class Prediction {
    public String sign;
    public String prediction;
};

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/prediction")
public class ZodiacController {
    private final ZodiacRepository zodiacRepository;

    public ZodiacController(ZodiacRepository zodiacRepository) {
        this.zodiacRepository = zodiacRepository;
    }

    @GetMapping("{sign}")
    public String getPrediction(@PathVariable String sign) {

        Optional<Zodiac> maybeZodiac = zodiacRepository.findByName(sign);

        System.out.println(maybeZodiac);

        if (maybeZodiac.isPresent()) {
            return maybeZodiac.get().getCurrent_prediction();
        }

        return "not found";

    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/update")
    public String updatePrediction(@RequestBody Prediction prediction) {
        System.out.print(prediction.prediction);
        zodiacRepository.deleteByName(prediction.sign);
        Zodiac zodiac = new Zodiac(prediction.sign, prediction.prediction);
        zodiacRepository.save(zodiac);
        return "updated";
    }
}
