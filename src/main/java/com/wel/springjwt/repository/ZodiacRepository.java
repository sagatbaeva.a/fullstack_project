package com.wel.springjwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wel.springjwt.models.Zodiac;
import javax.transaction.Transactional;

@Repository
public interface ZodiacRepository extends JpaRepository<Zodiac, Long> {
    Optional<Zodiac> findByName(String name);

    @Transactional
    void deleteByName(String name);
}
