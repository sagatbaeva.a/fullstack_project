package com.wel.springjwt.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;

@Entity
@Table(name = "zodiac_signs")
public class Zodiac {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "name")
    @Size(max = 50)
    private String name;

    @Column(name = "prediction")
    private String current_prediction;

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String getCurrent_prediction() {
        return current_prediction;
    }

    void setCurrent_prediction(String current_prediction) {
        this.current_prediction = current_prediction;
    }

    public Zodiac(String name, String current_prediction) {
        this.name = name;
        this.current_prediction = current_prediction;
    }

    public Zodiac() {
    }

}
