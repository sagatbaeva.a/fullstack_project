import * as React from "react";
import SignsRow from "./SignsRow";
import { Box } from "@mui/system";
import { TextField } from "@mui/material";
import { Typography } from "@mui/material";
import { Button } from "@mui/material";
import { useState } from "react";
import { useSelector } from "react-redux";
import AuthService from "../services/auth";
import { useNavigate } from "react-router-dom";
import { NavigateFunction } from "react-router";
import { useEffect } from "react";

export default function SubmitPrediction() {
  let current_zodiac = useSelector((state: any) => state.current_zodiac);
  const [prediction, setPrediction] = useState("");
  let navigate: NavigateFunction = useNavigate();
  useEffect(() => {
    if (!AuthService.isSignedIn()) {
      navigate("/login");
      window.location.reload();
    }
  }, []);
  return (
    <>
      <SignsRow creation />
      <Box
        sx={{
          height: "100%",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
        alignContent="center"
      >
        <Box
          sx={{
            width: "100%",
            alignItems: "center",
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
          }}
          alignContent="center"
        >
          <Box
            sx={{
              height: "100%",
              width: "100%",
              alignItems: "center",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
            }}
            alignContent="center"
          >
            <Box
              sx={{
                width: "50%",
                alignItems: "left",
                display: "flex",
                flexDirection: "row",
                justifyContent: "left",
                alignContent: "left",
              }}
            >
              <Typography variant="h6" sx={{ color: "white" }}>
                {current_zodiac === "default"
                  ? "Выберите знак зодиака, для которого будет написано предсказание"
                  : `предсказание для: ${current_zodiac}`}
              </Typography>
            </Box>
            <TextField
              sx={{
                width: "50%",
                border: "solid",
                borderColor: "primary.main",
                borderRadius: "5px",
              }}
              multiline
              rows={3}
              inputProps={{ style: { color: "white" } }}
              onChange={(e) => setPrediction(e.target.value)}
              value={prediction}
            ></TextField>
            <Button
              variant="outlined"
              sx={{ margin: "10px 0" }}
              size="large"
              onClick={() => {
                if (current_zodiac === "default") {
                  return;
                }
                setPrediction("");
                const user = JSON.parse(localStorage.getItem("user") || "{}");
                fetch(`${AuthService.API_URL}api/prediction/update/`, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + user.accessToken,
                  },
                  body: JSON.stringify({
                    sign: current_zodiac,
                    prediction: prediction,
                  }),
                });
              }}
            >
              Создать
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
}
