import * as React from "react";
import { Button } from "@mui/material";
import { change_zodiac } from "./CurrentZodiac";
import { useDispatch } from "react-redux";
import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";

type ZodiacButtonProps = {
  sign: string;
};

export default function ZodiacButton(props: any) {
  const dispatch = useDispatch();
  return (
    <Box sx={{ m: "7px" }}>
      <Button
        onClick={() => {
          dispatch(change_zodiac(props.sign));
        }}
      >
        <Typography variant="h5">{props.sign}</Typography>
      </Button>
    </Box>
  );
}
