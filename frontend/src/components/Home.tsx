import * as React from "react";
import SignsRow from "./SignsRow";
import Prediction from "./Prediction";

export default function Home() {
  return (
    <>
      <SignsRow creation={false} />
      <Prediction />
    </>
  );
}
