import * as React from "react";
import { useState, useEffect } from "react";
import { Typography } from "@mui/material";
import { useCallback } from "react";
import Box from "@mui/material/Box";
import AuthService from "../services/auth";
import { useSelector } from "react-redux";

export default function Prediction() {
  const zodiac = useSelector((state: any) => state.current_zodiac);
  const [loading, setLoading] = useState(true);
  const [prediction, setPrediction] = useState("");

  useEffect(() => {
    console.log(`${AuthService.API_URL}api/prediction/${zodiac}`);
    fetch(`${AuthService.API_URL}api/prediction/${zodiac}`)
      .then((response) => response.text())
      .then((data) => {
        setPrediction(data);
        setLoading(false);
      });
  }, [zodiac]);

  if (loading) {
    return <div>Loading...</div>;
  }

  const real_zodiac = zodiac === "default" ? "" : zodiac;
  let real_prediction =
    zodiac === "default"
      ? "Выберите знак зодиака, чтобы получить предсказание"
      : prediction;

  if (real_prediction === "not found") {
    real_prediction = "Предсказание не найдено";
  }
  return (
    <Box
      sx={{
        height: "100%",
        alignItems: "center",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
      alignContent="center"
    >
      <Box
        sx={{
          width: "100%",
          alignItems: "center",
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
        alignContent="center"
      >
        <Box
          sx={{
            border: "solid",
            borderColor: "primary.main",
            borderRadius: "5px",
            padding: "20px",
            margin: "10px 0",
            textAlign: "center",
            width: "50%",
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
          }}
          alignItems="center"
        >
          <Typography variant="h2">{real_zodiac}</Typography>
          <Typography variant="h4">{real_prediction}</Typography>
        </Box>
      </Box>
    </Box>
  );
}
