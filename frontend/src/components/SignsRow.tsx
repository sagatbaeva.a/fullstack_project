import ZodiacButton from "./ZodiacButton";
import Box from "@mui/material/Box";
import * as React from "react";
import Button from "@mui/material/Button";
import Avatar from "@mui/material/Avatar";
import { Link as RouterLink } from "react-router-dom";
import AuthService from "../services/auth";

type ZodiacButtonProps = {
  creation: boolean;
};

export default function SignsRow(props: ZodiacButtonProps) {
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        width: "100%",
      }}
    >
      <ZodiacButton sign="Овен" />
      <ZodiacButton sign="Телец" />
      <ZodiacButton sign="Близнецы" />
      <ZodiacButton sign="Рак" />
      <ZodiacButton sign="Лев" />
      <ZodiacButton sign="Дева" />
      <ZodiacButton sign="Весы" />
      <ZodiacButton sign="Скорпион" />
      <ZodiacButton sign="Стрелец" />
      <ZodiacButton sign="Козерог" />
      <ZodiacButton sign="Водолей" />
      <ZodiacButton sign="Рыбы" />
      {!AuthService.isSignedIn() && (
        <RouterLink to="/login">
          <Button startIcon={<Avatar />}></Button>
        </RouterLink>
      )}
      {!props.creation && AuthService.isSignedIn() && (
        <RouterLink to="/submit">
          <Box sx={{ m: "7px" }}>
            <Button variant="contained">Создать</Button>
          </Box>
        </RouterLink>
      )}
      {props.creation && AuthService.isSignedIn() && (
        <RouterLink to="/">
          <Box sx={{ m: "7px" }}>
            <Button variant="contained">В начало</Button>
          </Box>
        </RouterLink>
      )}
    </Box>
  );
}
