import { configureStore } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";

const predictionSlice = createSlice({
  name: "current_zodiac",
  initialState: {
    current_zodiac: "default",
  },
  reducers: {
    change_zodiac: (state, action) => {
      state.current_zodiac = action.payload;
    },
  },
});

export const { change_zodiac } = predictionSlice.actions;

export default configureStore({
  reducer: predictionSlice.reducer,
});
